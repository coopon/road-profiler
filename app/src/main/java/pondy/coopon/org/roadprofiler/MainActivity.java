package pondy.coopon.org.roadprofiler;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hs.gpxparser.GPXWriter;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


public class MainActivity extends AppCompatActivity {
    static final String storageLocation = "RoadProfiler";
    static final int GPS_ON = 1;

    final Track gpxTrack = new Track();                 // Instantiate GPX Track

    LocationManager locationManager;                    // Instantiate Location Manager
    LocationListener locationListener;                  // Instantiate Location Event Listener

    SensorManager mSensorManager;                       // Instantiate Sensor Manager
    SensorEventListener sensorEventListener;            // Instantiate Sensor Event Listener
    Sensor mSensor;                                     // Variable to hold the measurand from Sensor register

    Handler handler = new Handler();                    // Instantiate a Handler

    AlertDialog alert;                                  // For alerts

    Button startRecording;                              // 2 Buttons - for Starting, and Stopping
    Button stopRecording;

    TextView acc_x;                                     // 3 Variables for accelerometer Data @ X,Y,Z directions respectively
    TextView acc_y;
    TextView acc_z;

    TextView ref_x;                                     // 3 Ref.Variables for accelerometer Data @ X,Y,Z directions respectively
    TextView ref_y;
    TextView ref_z;

    TextView currSpeed;                                 //  to display current calculated Velocity (Speed)
    TextView avgSpeed;                                  //  to display average of speed throughout the travel distance/duration
    TextView distance;                                  //  to display complete distance covered

    TextView iriTextView;                               // to display International Roughness Index
    TextView timeTextView;                              // to display time travelled

    Boolean isTrialRun = false;                         // For calibration
    Boolean isRecording = false;                        // For recording trip

    ArrayList<Float> speedPoints = new ArrayList<>();               // Array to store instantaneous speed values
    ArrayList<Waypoint> trackPoints = new ArrayList<>();            //       to store Waypoints from GPS
    ArrayList<Float> verticalAcceleration = new ArrayList<>();      //       to store Vertical Acceleration from Acclerometer Sensor
    ArrayList<float[]> initialAccReadings = new ArrayList<>();      //       to store Acceleration value @ rest, due to Gravity

    float[] refAccVector;                               // To store final Reference Acceleration Vector
    float totalDistanceTravelled = 0;                   // Total distance
    float averageSpeed = 0;                             // Average Speed through the distance
    float IRI = 0;                                      // Initializing International Roughness Index

    Date startTime;                                     // For TimeStamping and Speed calculation
    Date endTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Invoke location Permission if not already granted using Location Manager */
        if (!LocationUtils.checkLocationPermission(getApplicationContext())){
            LocationUtils.requestLocationPermission(this);
        } else
            setLocationManager();

        setSensorManager();                             // Invoke Sensor Manager

        /* Acc. live TextViews */
        acc_x = findViewById(R.id.acc_x);
        acc_y = findViewById(R.id.acc_y);
        acc_z = findViewById(R.id.acc_z);

        /* reference vector TextViews */
        ref_x = findViewById(R.id.ref_x);
        ref_y = findViewById(R.id.ref_y);
        ref_z = findViewById(R.id.ref_z);

        currSpeed = findViewById(R.id.current_speed);
        avgSpeed = findViewById(R.id.avg_speed);
        distance = findViewById(R.id.distance);

        iriTextView = findViewById(R.id.iri);
        timeTextView = findViewById(R.id.time_travelled);

        startRecording = findViewById(R.id.start_recording);            // On Start Event :
                                                                        // Acquire location, accelerometer values, update reference vector
        startRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGPXRecording();
                handler.post(gpsUpdateCode);

                refAccVector = new float[3];

                if (mSensorManager != null) {
                    isTrialRun = true;
                    handler.postDelayed(stopAccelerationData, 5000);
                }
            }
        });

        stopRecording = findViewById(R.id.stop_recording);              // On Stop Event :
                                                                        // Stop acquisition of updates from respective listeners, managers
        stopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableRecordingButtons();
                endTime = new Date();

                handler.removeCallbacks(gpsUpdateCode);
                if (locationListener != null && locationManager != null)
                    locationManager.removeUpdates(locationListener);

                pauseGPXRecording();
                // writeGPXFile();

                /* time in seconds */
                float timeTravelled = (endTime.getTime() - startTime.getTime()) / (1000);

                /* average speed travelled */
                for (float speed: speedPoints)
                    averageSpeed += speed;

                averageSpeed = averageSpeed/speedPoints.size();
                totalDistanceTravelled = (timeTravelled * averageSpeed);

                IRI = RoadProfiler.computeIRI(verticalAcceleration, 1, totalDistanceTravelled/1000);
                Log.i("Computed IRI", Float.toString(IRI));

                String timeUnits = "seconds";
                String distanceUnits = "m";

                if (totalDistanceTravelled > 999) {         // Unit conversion from m to km after 1000 m
                    distanceUnits = "km";
                    totalDistanceTravelled /= 1000;
                }

                if (timeTravelled > 59) {                   // Unit conversion from seconds to minutes after 60 seonds
                    timeUnits = "minutes";
                    timeTravelled /= 60;
                }

                avgSpeed.setText(String.format(Locale.ENGLISH, "%.2f km/h", averageSpeed*3.6));
                distance.setText(String.format(Locale.ENGLISH, "%.2f " + distanceUnits, totalDistanceTravelled));
                timeTextView.setText(String.format(Locale.ENGLISH, "%.2f " + timeUnits, timeTravelled));
                iriTextView.setText(String.format(Locale.ENGLISH, "%.2f", IRI));
            }
        });
    }

    private void setLocationManager () {
                                                                    // Use Location Manager to invoke the System Location Service
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
                                                                    // Get Speed at current location, after initiating recording
            @Override
            public void onLocationChanged(Location location) {
                if (!startRecording.isEnabled())
                    enableRecordingButtons();
                currSpeed.setText(String.format(Locale.ENGLISH, "%.2f m/s", location.getSpeed()));
            }

                                                                    // Manage location Provider Status
            @Override
            public void onStatusChanged(String provider, int status, Bundle bundle) {
                String statusMsg = provider;
                if (status == LocationProvider.AVAILABLE) {
                    startGPXRecording();
                    statusMsg += " is now available";
                } else if (status == LocationProvider.OUT_OF_SERVICE) {
                    pauseGPXRecording();
                    statusMsg += " is out of service";
                } else {
                    pauseGPXRecording();
                    statusMsg += " is temporarily not available";
                }

                Toast.makeText(MainActivity.this, statusMsg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderEnabled(String provider) {
                Toast.makeText(MainActivity.this, provider+" enabled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(MainActivity.this, provider+" disabled" , Toast.LENGTH_SHORT).show();
                pauseGPXRecording();
                disableRecordingButtons();
                showSettingsAlert();
            }
        };

                                                                    // Update location @ 1 Hz Sampling frequency
        if (LocationUtils.checkLocationPermission(getApplicationContext()))
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
    }

    private void setSensorManager() {

                                                                    // invoke Sensor Manager from Sensor System Service
        if (mSensorManager == null)
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (mSensor == null)                                        // Set which sensor to acquire data from
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        sensorEventListener = new SensorEventListener() {
            @Override                                               // Acquire data on Sensor Value change event (non cpu demanding)
            public void onSensorChanged(SensorEvent sensorEvent) {

                Log.i("Sensor Values:", sensorEvent.values.toString());

                acc_x.setText(String.format(Locale.ENGLISH, "%.3f", sensorEvent.values[0]));
                acc_y.setText(String.format(Locale.ENGLISH, "%.3f", sensorEvent.values[1]));
                acc_z.setText(String.format(Locale.ENGLISH, "%.3f", sensorEvent.values[2]));


                if (isTrialRun) {                                   // Acquire the sensor values to use them for deriving reference values
                    initialAccReadings.add(sensorEvent.values);
                }

                if (!isTrialRun && isRecording) {                                  // Acquire the sensor values and derive Vertical displacement derivatives
                    verticalAcceleration.add(
                            RoadProfiler.computeVerticalAcceleration(sensorEvent.values, refAccVector)
                    );
                }
            }

            @Override                                               // Change Accuracy ?
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
                                                                    // Connect event handler to Sensor @ particular Sampling Frequency
        mSensorManager.registerListener(sensorEventListener, mSensor, 1000000);
    }

    private Runnable stopAccelerationData = new Runnable() {
        @Override
        public void run() {
            if (isTrialRun) {                                   // Derive Reference Vector during Trial Run
                isTrialRun = !isTrialRun;

                refAccVector = RoadProfiler.computeAverage(initialAccReadings);
                startTime = new Date();

                ref_x.setText(String.format(Locale.ENGLISH, "%.3f", refAccVector[0]));
                ref_y.setText(String.format(Locale.ENGLISH, "%.3f", refAccVector[1]));
                ref_z.setText(String.format(Locale.ENGLISH, "%.3f", refAccVector[2]));

                Toast.makeText(MainActivity.this, "Ready To Go.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private Runnable gpsUpdateCode = new Runnable() {
        @Override                                               // Update GPS - Location
        public void run() {
            if (LocationUtils.checkLocationPermission(getApplicationContext())) {
                Location recentlyKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                                                                // Get all three dimension Values : Latitude, Longitude, Relative Altitude
                Waypoint point = new Waypoint(recentlyKnownLocation.getLatitude(), recentlyKnownLocation.getLongitude());
                point.setElevation(recentlyKnownLocation.getAltitude());
                point.setTime(new Date());

                if (recentlyKnownLocation.hasSpeed())           // Derive Speed
                    point.addExtensionData("speed", recentlyKnownLocation.getSpeed());

                if (recentlyKnownLocation.hasSpeed())           // Speed @ particular location
                    speedPoints.add(recentlyKnownLocation.getSpeed());

                if (isRecording)                                // Use it for deriving GPX Track
                    trackPoints.add(point);
                handler.postDelayed(this, 2000);
            }

        }
    };


    @Override                                                   // Negotiate to Access Location Service
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LocationUtils.LOCATION_PERMISSION)
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
                setLocationManager();
            } else {
                LocationUtils.requestLocationPermission(this);
            }
    }

    private void showSettingsAlert() {                          // Location access UX Feedback
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setMessage("Location is turned off. Turn it on now?");
        alertDialogBuilder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, GPS_ON);
                alert.dismiss();
            }
        });
        if (alert == null)
            alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override                                                   // Give responsive Feedback on Disabled Location Access
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GPS_ON) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                showSettingsAlert();
        }
    }

    private void enableRecordingButtons() {                     // State of UI Buttons
        startRecording.setEnabled(true);
        stopRecording.setEnabled(true);
    }

    private void disableRecordingButtons() {
        startRecording.setEnabled(false);
        stopRecording.setEnabled(false);
    }

    private void startGPXRecording() {                          // State of GPX Recording
        if (!isRecording)
            isRecording = true;
    }

    private void pauseGPXRecording() {
        if (isRecording)
            isRecording = false;
    }

    private void writeGPXFile() {                               // FILE I/O Handling to create, append GPX Track Stream
        GPX gpx = new GPX();
        gpx.setCreator("Road Profiler");

        TrackSegment trackSegment = new TrackSegment();
        trackSegment.setWaypoints(trackPoints);
        gpxTrack.addTrackSegment(trackSegment);
        gpx.addTrack(gpxTrack);

        try {
            File storageFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), storageLocation);
            if (!storageFolder.exists())
                if (!storageFolder.mkdirs())
                    return;

            GPXWriter gpxParser = new GPXWriter();
            FileOutputStream fileOutputStream = new FileOutputStream(storageFolder.toString() + "/output.gpx");
            gpxParser.writeGPX(gpx, fileOutputStream);
            fileOutputStream.close();

        } catch (FileNotFoundException ex) {
        } catch (ParserConfigurationException pex) {
        } catch (IOException ioex) {
        } catch (TransformerException tex) {
        }
    }
}
